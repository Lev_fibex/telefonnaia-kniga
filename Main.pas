unit Main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, sListView, sPageControl,
  Vcl.Menus, System.ImageList, Vcl.ImgList, Vcl.ExtCtrls,
  sPanel, Vcl.StdCtrls, sLabel, acImage, acPNG,System.RegularExpressions,
  Vcl.Buttons, sSpeedButton, sEdit,System.Hash;

type
  TMainForm = class(TForm)
    Info_PageControl: TsPageControl;
    Persons_TabSheet: TsTabSheet;
    Persons_ListView: TsListView;
    Persons_PopupMenu: TPopupMenu;
    Status_ImageList: TImageList;
    Internal_number: TMenuItem;
    Mobile_number: TMenuItem;
    Home_number: TMenuItem;
    User_Panel: TsPanel;
    Number_Label: TsLabel;
    User_Label: TsLabel;
    sImage1: TsImage;
    Contact_TabSheet: TsTabSheet;
    Contact_ListView: TsListView;
    Contact_PopupMenu: TPopupMenu;
    Contact_mobile_number: TMenuItem;
    Contact_home_number: TMenuItem;
    Contact_Tools_Panel: TsPanel;
    Change_Contact_SpeedButton: TsSpeedButton;
    Delete_Contact_SpeedButton: TsSpeedButton;
    Call_Contact_SpeedButton: TsSpeedButton;
    Add_Contact_SpeedButton: TsSpeedButton;
    Persons_Tools_Panel: TsPanel;
    Call_Person_SpeedButton: TsSpeedButton;
    Find_Contact_Edit: TsEdit;
    Find_Persons_Edit: TsEdit;
    procedure FormCreate(Sender: TObject);
    function Connect():String;
    function DomainName(domain:string):String;
    function GetDomain():String;
    function Authorization_AMI(ID:string):String;
    function Originate(id,channel,exten,context,priority:string):String;
    procedure Persons_PopupMenuPopup(Sender: TObject);
    procedure Internal_numberClick(Sender: TObject);
    procedure Mobile_numberClick(Sender: TObject);
    procedure Home_numberClick(Sender: TObject);
    procedure Contact_PopupMenuPopup(Sender: TObject);
    procedure Contact_TabSheetShow(Sender: TObject);
    procedure Contact_mobile_numberClick(Sender: TObject);
    procedure Contact_home_numberClick(Sender: TObject);
    procedure Change_ContactClick(Sender: TObject);
    procedure Delete_ContactClick(Sender: TObject);
    procedure Add_Contact_SpeedButtonClick(Sender: TObject);
    procedure Call_Contact_SpeedButtonClick(Sender: TObject);
    procedure Call_Person_SpeedButtonClick(Sender: TObject);
    procedure Find_Contact_EditChange(Sender: TObject);
    procedure Persons_TabSheetShow(Sender: TObject);
    procedure Find_Persons_EditChange(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;
  subcriber_number,outgoing_context,organization:string;
  LDAP_host,account_name:string;
  AMI_Password,AMI_User:string;

implementation

{$R *.dfm}

uses Datamodule, Contact;



procedure TMainForm.Find_Contact_EditChange(Sender: TObject);
begin
      Contact_TabSheetShow(Find_Contact_Edit);
end;
procedure TMainForm.Find_Persons_EditChange(Sender: TObject);
begin
     Persons_TabSheetShow(Find_Persons_Edit);
end;
procedure TMainForm.FormCreate(Sender: TObject);
begin
     Connect();
end;
procedure TMainForm.Home_numberClick(Sender: TObject);
begin
     Originate('call_client','SIP/'+subcriber_number,Persons_ListView.Selected.SubItems[4],outgoing_context,'1');
end;
procedure TMainForm.Internal_numberClick(Sender: TObject);
begin
     Originate('call_client','SIP/'+subcriber_number,Persons_ListView.Selected.SubItems[2],outgoing_context,'1');
end;
procedure TMainForm.Mobile_numberClick(Sender: TObject);
begin
     Originate('call_client','SIP/'+subcriber_number,Persons_ListView.Selected.SubItems[3],outgoing_context,'1');
end;
procedure TMainForm.Persons_PopupMenuPopup(Sender: TObject);
begin
     if Persons_ListView.Selected<>nil then
       begin
            if Persons_ListView.Selected.SubItems[2]>'' then
              begin
                   Internal_number.Caption:='���������� �����: '+Persons_ListView.Selected.SubItems[2];
                   Internal_number.Visible:=true;
              end
            else
              begin
                   Internal_number.Visible:=false;
              end;

            if Persons_ListView.Selected.SubItems[3]>'' then
              begin
                   Mobile_number.Caption:='��������� �����: '+Persons_ListView.Selected.SubItems[3];
                   Mobile_number.Visible:=true;
               end
            else
              begin
                   Mobile_number.Visible:=false;
              end;

            if Persons_ListView.Selected.SubItems[4]>'' then
              begin
                   Home_number.Caption:='�������� �����: '+Persons_ListView.Selected.SubItems[4];
                   Home_number.Visible:=true;
              end
            else
              begin
                   Home_number.Visible:=false;
              end;
       end
     else
       begin
            Internal_number.Visible:=false;
            Mobile_number.Visible:=false;
            Home_number.Visible:=false;
       end;
end;
procedure TMainForm.Persons_TabSheetShow(Sender: TObject);
var
query: string;
count:integer;
begin
     count:=0;
     if Find_Persons_Edit.Text='' then
       begin
            query:='';
       end
     else
       begin
            Datamodule.Data.ADOQuery.Filter:='';
            query:=' AND (name= '+#39+'*'+Find_Persons_Edit.Text+'*'+#39+' OR title ='+#39+'*'+Find_Persons_Edit.Text+'*'+#39+')';
       end;


     Persons_ListView.Items.BeginUpdate;
     Persons_ListView.Items.Clear;

     Datamodule.Data.ADOQuery.SQL.Clear;
     Datamodule.Data.ADOQuery.SQL.Add('select  homephone,mobile,telephonenumber,title,name FROM '#39'LDAP://'+LDAP_host+''#39'');
     Datamodule.Data.ADOQuery.SQL.Add('WHERE objectcategory = '+#39+'Person'+#39+' AND objectclass = '+#39+'User'+#39+' AND company = '+#39+organization+#39+' AND sAMAccountName<> '+#39+account_name+#39
     +query);
     Datamodule.Data.ADOQuery.Open;
     Datamodule.Data.ADOQuery.First;

     while not Datamodule.Data.ADOQuery.EOF do
          begin
               with Persons_ListView.Items.Add do
                   begin
                        ImageIndex:=0;
                        SubItems.Add(Datamodule.Data.ADOQuery.Fields[0].AsString);
                        SubItems.Add(Datamodule.Data.ADOQuery.Fields[1].AsString);
                        SubItems.Add(Datamodule.Data.ADOQuery.Fields[2].AsString);
                        SubItems.Add(Datamodule.Data.ADOQuery.Fields[3].AsString);
                        SubItems.Add(Datamodule.Data.ADOQuery.Fields[4].AsString);
                   end;
               if count>=20 then
                  begin
                       break;
                  end
                  else
                  begin
                       count:=count+1;
                  end;
               Datamodule.Data.ADOQuery.Next;

          end;

     Persons_ListView.Items.EndUpdate;
     Datamodule.Data.ADOQuery.Active:=false;
end;
procedure TMainForm.Add_Contact_SpeedButtonClick(Sender: TObject);
begin
     if (not Assigned(ContactForm)) then ContactForm:=TContactForm.Create(Self);
     ContactForm.type_contact:='new';
     ContactForm.Show;
end;
procedure TMainForm.Call_Contact_SpeedButtonClick(Sender: TObject);
begin
     Contact_PopupMenu.Popup(Mouse.CursorPos.X, Mouse.CursorPos.Y);
end;
procedure TMainForm.Call_Person_SpeedButtonClick(Sender: TObject);
begin
     Persons_PopupMenu.Popup(Mouse.CursorPos.X, Mouse.CursorPos.Y);
end;
procedure TMainForm.Change_ContactClick(Sender: TObject);
begin
     if Contact_ListView.Selected<>nil then
       begin
            if (not Assigned(ContactForm)) then ContactForm:=TContactForm.Create(Self);
            ContactForm.Display_Name_Edit.Text:=Contact_ListView.Selected.SubItems[0];
            ContactForm.Title_Edit.Text:=Contact_ListView.Selected.SubItems[1];
            ContactForm.Last_Name_Edit.Text:=Contact_ListView.Selected.SubItems[4];
            ContactForm.First_Name_Edit.Text:=Contact_ListView.Selected.SubItems[5];
            ContactForm.Second_Name_Edit.Text:=Contact_ListView.Selected.SubItems[6];
            ContactForm.Mobile_Number_Edit.Text:=Contact_ListView.Selected.SubItems[2];
            ContactForm.Home_Number_Edit.Text:=Contact_ListView.Selected.SubItems[3];
            ContactForm.type_contact:='change';
            ContactForm.number_contact:=Contact_ListView.Selected.SubItems[7];
            ContactForm.Show;
       end;
end;
function TMainForm.Connect():String;
var
i:integer;
LDAP_user,LDAP_password:string;
f:TstringList;
begin
     LDAP_host:='';
     LDAP_user:='';
     LDAP_password:='';
     AMI_Password:='';
     AMI_User:='';

     f:=TStringList.Create();
     f.LoadFromFile('client.conf');

     Datamodule.Data.IdTCPClient.Host:=f.Values['AMI_host'];
     AMI_User:=f.Values['AMI_username'];
     AMI_Password:=f.Values['AMI_password'];

     LDAP_host:=f.Values['LDAP_host'];
     LDAP_user:=f.Values['LDAP_username'];
     LDAP_password:=f.Values['LDAP_password'];

     outgoing_context:=f.Values['outgoing_context'];
     f.Free;


     if LDAP_host<>'' then
       begin
            account_name:=Copy(LDAP_user,Pos('\',LDAP_user)+1,length(LDAP_user));

            Datamodule.Data.ADOConnection.ConnectionString:='Provider=ADsDSOObject;Password='+LDAP_password+';User ID='+LDAP_user+';Encrypt Password=False;Mode=Read;Bind Flags=0;ADSI Flag=-2147483648';
       end
     else
       begin
            LDAP_host:=DomainName(GetDomain());
            account_name:=GetEnvironmentVariable('USERNAME');
            Datamodule.Data.ADOConnection.ConnectionString:='Provider=ADsDSOObject;Encrypt Password=False;Integrated Security=SSPI;Mode=Read;Bind Flags=0;ADSI Flag=-2147483648';
       end;

     //�����������.
     if Datamodule.Data.IdTCPClient.Connected=true then
       begin
            Datamodule.Data.IdTCPClient.Disconnect;
            if not Datamodule.Data.IdTCPClient.IOHandler.InputBufferIsEmpty then Datamodule.Data.IdTCPClient.IOHandler.InputBuffer.Clear;
       end;
     if Datamodule.Data.ADOConnection.Connected=true then Datamodule.Data.ADOConnection.Connected:=false;

     try
        Datamodule.Data.IdTCPClient.Connect;
        Authorization_AMI('connect');
        except
              MessageBox(Handle,'��� ���������� � AMI','AMI',MB_OK+MB_ICONASTERISK+MB_SYSTEMMODAL);
     end;

     try
        Datamodule.Data.ADOConnection.Connected:=true;
        except
              MessageBox(Handle,'��� ���������� � LDAP','LDAP',MB_OK+MB_ICONASTERISK+MB_SYSTEMMODAL);
     end;

     Datamodule.Data.ADOQuery.SQL.Clear;
     Datamodule.Data.ADOQuery.SQL.Add('select company,telephonenumber,name FROM '#39'LDAP://'+LDAP_host+''#39'');
     Datamodule.Data.ADOQuery.SQL.Add('WHERE objectcategory = '+#39+'Person'+#39+' AND objectclass = '+#39+'User'+#39+' AND sAMAccountName= '+#39+account_name+#39);
     Datamodule.Data.ADOQuery.Open;
     Datamodule.Data.ADOQuery.First;
     User_Label.Caption:=Datamodule.Data.ADOQuery.Fields[0].AsString;
     subcriber_number:=Datamodule.Data.ADOQuery.Fields[1].AsString;
     organization:=Datamodule.Data.ADOQuery.Fields[2].AsString;

     Number_Label.Caption:=subcriber_number+'@'+organization;
     Datamodule.Data.ADOQuery.Active:=false;
end;
procedure TMainForm.Contact_home_numberClick(Sender: TObject);
begin
     Originate('call_client','SIP/'+subcriber_number,Contact_ListView.Selected.SubItems[3],outgoing_context,'1');
end;
procedure TMainForm.Contact_mobile_numberClick(Sender: TObject);
begin
     Originate('call_client','SIP/'+subcriber_number,Contact_ListView.Selected.SubItems[2],outgoing_context,'1');
end;
procedure TMainForm.Contact_PopupMenuPopup(Sender: TObject);
begin
     if Contact_ListView.Selected<>nil then
       begin
            if Contact_ListView.Selected.SubItems[2]>'' then
              begin
                   Contact_mobile_number.Caption:='��������� �����: '+Contact_ListView.Selected.SubItems[2];
                   Contact_mobile_number.Visible:=true;
              end
              else
              begin
                   Contact_mobile_number.Visible:=false;
              end;

            if Contact_ListView.Selected.SubItems[3]>'' then
              begin
                   Contact_home_number.Caption:='�������� �����: '+Contact_ListView.Selected.SubItems[3];
                   Contact_home_number.Visible:=true;
              end
            else
              begin
                  Contact_home_number.Visible:=false;
              end;
       end
     else
       begin
            Contact_mobile_number.Visible:=false;
            Contact_home_number.Visible:=false;
       end;
end;
procedure TMainForm.Contact_TabSheetShow(Sender: TObject);
var
query: string;
count:Integer;
begin
     count:=0;
     query:=Find_Contact_Edit.Text;
     Contact_ListView.Items.BeginUpdate;
     Contact_ListView.Items.Clear;

     Datamodule.Data.ADOQuery_DB.SQL.Clear;
     Datamodule.Data.ADOQuery_DB.SQL.Add('select * FROM contact WHERE Display LIKE ''%'+query+'%''OR Last_name LIKE ''%'+query+
     '%''OR First_name LIKE ''%'+query+'%''OR Second_name LIKE ''%'+query+'%''OR title LIKE ''%'+query+'%''');
     Datamodule.Data.ADOQuery_DB.Open;
     Datamodule.Data.ADOQuery_DB.First;

     while not Datamodule.Data.ADOQuery_DB.EOF do
          begin
               with Contact_ListView.Items.Add do
                   begin
                        ImageIndex:=0;
                        SubItems.Add(Datamodule.Data.ADOQuery_DB.Fields[1].AsString);
                        SubItems.Add(Datamodule.Data.ADOQuery_DB.Fields[5].AsString);
                        SubItems.Add(Datamodule.Data.ADOQuery_DB.Fields[6].AsString);
                        SubItems.Add(Datamodule.Data.ADOQuery_DB.Fields[7].AsString);
                        SubItems.Add(Datamodule.Data.ADOQuery_DB.Fields[2].AsString);
                        SubItems.Add(Datamodule.Data.ADOQuery_DB.Fields[3].AsString);
                        SubItems.Add(Datamodule.Data.ADOQuery_DB.Fields[4].AsString);
                        SubItems.Add(Datamodule.Data.ADOQuery_DB.Fields[0].AsString);
                   end;
                if count>=20 then
                  begin
                       break;
                  end
                  else
                  begin
                       count:=count+1;
                  end;
               Datamodule.Data.ADOQuery_DB.Next;
          end;

     Contact_ListView.Items.EndUpdate;
     Datamodule.Data.ADOQuery_DB.Active:=false;
end;
procedure TMainForm.Delete_ContactClick(Sender: TObject);
begin
     if Contact_ListView.Selected<>nil then
       begin
            Datamodule.Data.ADOQuery_DB.SQL.Clear;
            Datamodule.Data.ADOQuery_DB.SQL.Add('Delete  FROM `contact` WHERE Code='+Contact_ListView.Selected.SubItems[7]+';');
            Datamodule.Data.ADOQuery_DB.ExecSQL;
            Datamodule.Data.ADOQuery_DB.Active:=false;

            Contact_TabSheetShow(Delete_Contact_SpeedButton);
       end;
end;
function TMainForm.DomainName(domain:string):String;
var
f:Tstrings;
i: integer;
domain_connect: string;
begin
     f:=TStringList.Create();
     f.text:=stringReplace(domain,'.',#13#10,[rfReplaceAll]);

     for i:=0 to f.Count-1 do
        begin
            if i=0 then domain_connect:=domain_connect+'DC='+f[i] else domain_connect:=domain_connect+',DC='+f[i];
        end;

     f.free;
     result:=domain_connect;
end;
function TMainForm.GetDomain():String;
var
  Domain : string;
  DomainLen : Dword;
begin
     DomainLen := 255;
     SetLength(Domain, DomainLen);
     GetComputerNameEx(ComputerNamePhysicalDnsDomain,PChar(Domain), DomainLen);
     result:=Domain;
end;
function TMainForm.Authorization_AMI(ID:string):String;
var
key,keymd5:string;
d: THashMD5;
begin
     Datamodule.Data.IdTCPClient.Socket.WriteLn('ActionID:login');
     Datamodule.Data.IdTCPClient.Socket.WriteLn('Action:Challenge');
     Datamodule.Data.IdTCPClient.Socket.WriteLn('AuthType:MD5');
     Datamodule.Data.IdTCPClient.Socket.WriteLn(#13#10);
     while 1>0 do
          begin

               if Datamodule.Data.IdTCPClient.Socket.ReadLn()<>'' then
                 begin
                      if Datamodule.Data.IdTCPClient.Socket.ReadLn()='ActionID: login' then break;
                      if Datamodule.Data.IdTCPClient.Socket.ReadLn()='Message: Authentication accepted' then  break;
                 end;
          end;

     d:= THashMD5.Create;
     key:=d.GetHashString(stringReplace(Datamodule.Data.IdTCPClient.Socket.ReadLn(),'Challenge: ','',[rfReplaceAll])+AMI_Password);
     Datamodule.Data.IdTCPClient.Socket.WriteLn('Action:login');
     Datamodule.Data.IdTCPClient.Socket.WriteLn('AuthType:MD5');
     Datamodule.Data.IdTCPClient.Socket.WriteLn('Username:'+AMI_User);
     Datamodule.Data.IdTCPClient.Socket.WriteLn('Key:'+key);
     Datamodule.Data.IdTCPClient.Socket.WriteLn('Events: on');
     Datamodule.Data.IdTCPClient.Socket.WriteLn(#13#10);

     while 1>0 do
          begin

               if Datamodule.Data.IdTCPClient.Socket.ReadLn()<>'' then
                 begin
                      if Datamodule.Data.IdTCPClient.Socket.ReadLn()='Message: Authentication accepted' then break;
                      if Datamodule.Data.IdTCPClient.Socket.ReadLn()='Message: Authentication failed' then  break;
                 end;
          end;
end;
function TMainForm.Originate(id,channel,exten,context,priority: string):String;
begin
     Datamodule.Data.IdTCPClient.Socket.WriteLn('ActionID:'+id);
     Datamodule.Data.IdTCPClient.Socket.WriteLn('Action:Originate');
     Datamodule.Data.IdTCPClient.Socket.WriteLn('Channel:'+channel);
     Datamodule.Data.IdTCPClient.Socket.WriteLn('Exten:'+exten);
     Datamodule.Data.IdTCPClient.Socket.WriteLn('Context:'+context);
     Datamodule.Data.IdTCPClient.Socket.WriteLn('Priority:'+priority);
     Datamodule.Data.IdTCPClient.Socket.WriteLn(#13#10);
end;
end.
