unit Datamodule;

interface

uses
  System.SysUtils, System.Classes, sSkinManager, Data.DB, Data.Win.ADODB,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient;

type
  TData = class(TDataModule)
    SkinManager: TsSkinManager;
    ADOConnection: TADOConnection;
    ADOQuery: TADOQuery;
    ADOConnection_DB: TADOConnection;
    ADOQuery_DB: TADOQuery;
    IdTCPClient: TIdTCPClient;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Data: TData;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

end.
