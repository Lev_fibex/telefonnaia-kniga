unit Contact;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, sGroupBox, sLabel, sEdit,
  Vcl.ExtCtrls, sPanel, sButton, System.ImageList, Vcl.ImgList;

type
  TContactForm = class(TForm)
    Panel: TsPanel;
    Display_Name_Edit: TsEdit;
    Display_Name_Label: TsLabel;
    Contact_GroupBox: TsGroupBox;
    Last_Name_Label: TsLabel;
    Last_Name_Edit: TsEdit;
    First_Name_Edit: TsEdit;
    Mobile_Number_Label: TsLabel;
    Home_Number_Label: TsLabel;
    First_Name_Label: TsLabel;
    Second_Name_Label: TsLabel;
    Title_Edit: TsEdit;
    Title_Label: TsLabel;
    Mobile_Number_Edit: TsEdit;
    Home_Number_Edit: TsEdit;
    Second_Name_Edit: TsEdit;
    Cancel_Button: TsButton;
    Save_Button: TsButton;
    Contact_ImageList: TImageList;
    procedure Cancel_ButtonClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Save_ButtonClick(Sender: TObject);
  private
    { Private declarations }
  public
       type_contact:string;
       number_contact:string;
  end;

var
  ContactForm: TContactForm;

implementation

{$R *.dfm}

uses Datamodule, Main;

procedure TContactForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     ContactForm:=nil;
     Action := caFree;
end;
procedure TContactForm.Save_ButtonClick(Sender: TObject);
begin
     if type_contact='change' then
       begin
            Datamodule.Data.ADOQuery_DB.SQL.Clear;
            Datamodule.Data.ADOQuery_DB.SQL.Add('UPDATE  `contact` SET Display="'+Display_Name_Edit.Text+'",Last_name="'+Last_Name_Edit.Text+'",First_name="'
            +First_Name_Edit.Text+'",Second_name="'+Second_Name_Edit.Text+'",title="'+Title_Edit.Text+'",mobile="'+Mobile_Number_Edit.Text+'",home_number="'
            +Home_Number_Edit.Text+'" WHERE Code='+number_contact+';');
            Datamodule.Data.ADOQuery_DB.ExecSQL;
            Datamodule.Data.ADOQuery_DB.Active:=false;
       end;
     if type_contact='new' then
       begin
            Datamodule.Data.ADOQuery_DB.SQL.Clear;
            Datamodule.Data.ADOQuery_DB.SQL.Add('INSERT INTO `contact` (Display,Last_name,First_name,Second_name,title,mobile,home_number)'+
            ' VALUES ("'+Display_Name_Edit.Text+'","'+Last_Name_Edit.Text+'","'+First_Name_Edit.Text+'","'+Second_Name_Edit.Text+'","'+Title_Edit.Text+'","'
            +Mobile_Number_Edit.Text+'","'+Home_Number_Edit.Text+'");');
            Datamodule.Data.ADOQuery_DB.ExecSQL;
            Datamodule.Data.ADOQuery_DB.Active:=false;
       end;
     MainForm.Contact_TabSheetShow(ContactForm.Save_Button);
     Close();
end;

procedure TContactForm.Cancel_ButtonClick(Sender: TObject);
begin
     Close;
end;

end.
