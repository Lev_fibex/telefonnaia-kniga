program Client;

uses
  Vcl.Forms,
  Main in 'Main.pas' {MainForm},
  Datamodule in 'Datamodule.pas' {Data: TDataModule},
  Contact in 'Contact.pas' {ContactForm};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
   Application.CreateForm(TData, Data);
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TContactForm, ContactForm);
  Application.Run;
end.
